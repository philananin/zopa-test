using FluentAssertions;
using Ninject;
using Xunit;
using ZopaTechTest.Infrastructure;
using ZopaTechTest.QuoteHandling;

namespace ZopaTechTest.Tests.QuoteHandling
{
    public class QuoteSystemEndToEndTests
    {
        [Fact]
        public void RunsExampleFromTestSpecification()
        {
            var quoteSystem = GetQuoteSystem();
            var result = quoteSystem.Run("market.csv", "1000");

            result.IsSuccessful.Should().BeTrue();
            result.Outcome.RequestedAmount.Should().Be("�1000");
            result.Outcome.Rate.Should().Be("7.0%");

            // I appreciate that these do not pass - I thought my formula was correct, but I cannot get it working!
            result.Outcome.MonthlyRepayment.Should().Be("�30.78");
            result.Outcome.TotalRepayment.Should().Be("�1108.10");
        }

        [Fact]
        public void ReturnsExpectedErrorMessageWhenFileNotFound()
        {
            var quoteSystem = GetQuoteSystem();
            var result = quoteSystem.Run("market2.csv", "1000");

            result.IsSuccessful.Should().BeFalse();
            result.Problems.ShouldBeEquivalentTo(new [] {"Market data file does not exist"});
        }

        private QuoteSystem GetQuoteSystem()
        {
            var kernel = new StandardKernel(new QuoteModule());
            return kernel.Get<QuoteSystem>();
        }
    }
}
