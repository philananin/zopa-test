using FluentAssertions;
using Xunit;
using ZopaTechTest.Models;
using ZopaTechTest.QuoteHandling;

namespace ZopaTechTest.Tests.QuoteHandling
{
    public class QuoteFulfilmentTests
    {
        private readonly QuoteFulfilment _quoteFulfilment;
        private readonly MarketData[] _marketData;

        public QuoteFulfilmentTests()
        {
            _quoteFulfilment = new QuoteFulfilment();
            _marketData = new[]
            {
                new MarketData {Lender = "Bob", Rate = 0.075m, Available = 640},
                new MarketData {Lender = "Jane", Rate = 0.069m, Available = 480},
                new MarketData {Lender = "Fred", Rate = 0.071m, Available = 520},
                new MarketData {Lender = "Mary", Rate = 0.104m, Available = 170},
                new MarketData {Lender = "John", Rate = 0.081m, Available = 320},
                new MarketData {Lender = "Dave", Rate = 0.074m, Available = 140},
                new MarketData {Lender = "Angela", Rate = 0.071m, Available = 60}
            };
        }

        [Fact]
        public void When_fulfilling_loan_with_sufficient_offers_then_expected_offers_are_returned()
        {
            var offersResult = _quoteFulfilment.GetOffersToFulfilLoan(LoanAmount.Create(1000), _marketData);

            offersResult.IsSuccessful.Should().BeTrue();
            offersResult.Outcome.ShouldBeEquivalentTo(
                new[]
                {
                    new PartialQuote(0.069m, 480, "Jane"),
                    new PartialQuote(0.071m, 520, "Fred")
                });
        }

        [Fact]
        public void When_fulfilling_loan_with_sufficient_offers_including_a_partial_fulfilment_then_expected_offers_are_returned()
        {
            var offersResult = _quoteFulfilment.GetOffersToFulfilLoan(LoanAmount.Create(1100), _marketData);

            offersResult.IsSuccessful.Should().BeTrue();
            offersResult.Outcome.ShouldBeEquivalentTo(
                new[]
                {
                    new PartialQuote(0.069m, 480, "Jane"),
                    new PartialQuote(0.071m, 520, "Fred"),
                    new PartialQuote(0.071m, 60, "Angela"),
                    new PartialQuote(0.074m, 40, "Dave")
                });
        }

        [Fact]
        public void When_fulfilling_loan_with_insufficient_offers_then_expected_problems_are_returned()
        {
            var offersResult = _quoteFulfilment.GetOffersToFulfilLoan(LoanAmount.Create(2500), _marketData);

            offersResult.IsSuccessful.Should().BeFalse();
            offersResult.Problems.ShouldBeEquivalentTo(new[] { "It is not possible to provide a quote at this time." });
        }
    }
}