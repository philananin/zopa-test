using System.Collections.Generic;
using FluentAssertions;
using Xunit;
using ZopaTechTest.Models;
using ZopaTechTest.QuoteHandling;

namespace ZopaTechTest.Tests.QuoteHandling
{
    public class QuoteCalculatorTests
    {
        private readonly QuoteCalculator _quoteCalculator;

        public QuoteCalculatorTests()
        {
            _quoteCalculator = new QuoteCalculator();
        }

        [Fact]
        public void When_calculating_using_example_data_calculates_correct_rate_and_amounts()
        {
            var partialQuotes = new List<PartialQuote>
            {
                new PartialQuote(0.069m, 480, "Jane"),
                new PartialQuote(0.071m, 520, "Fred")
            };
            var quote = _quoteCalculator.CalculateQuote(LoanAmount.Create(1000), partialQuotes);

            quote.RequestedAmount.Should().Be("�1000");
            quote.Rate.Should().Be("7.0%");

            // I appreciate that these do not pass - I thought my formula was correct, but I cannot get it working!
            quote.MonthlyRepayment.Should().Be("�30.78");
            quote.TotalRepayment.Should().Be("�1108.10");
        }
    }
}