﻿using FluentAssertions;
using Xunit;
using ZopaTechTest.ArgumentHandling;

namespace ZopaTechTest.Tests.ArgumentHandling
{
    public class QuoteArgumentParserTests
    {
        private readonly QuoteArgumentParser _quoteArgumentParser;

        public QuoteArgumentParserTests()
        {
            _quoteArgumentParser = new QuoteArgumentParser(new LoanAmountParser(), new MarketDataFileReader());
        }

        [Fact]
        public void When_parsing_valid_arguments_then_returns_success()
        {
            var result = _quoteArgumentParser.Parse(new [] {"market.csv", "1000"});

            result.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void When_parsing_null_then_returns_failure()
        {
            var result = _quoteArgumentParser.Parse(null);

            result.IsSuccessful.Should().BeFalse();
            result.Problems.ShouldBeEquivalentTo(new[] {"Must provide arguments: market data file, loan amount"});
        }

        [Fact]
        public void When_parsing_incorrect_number_of_arguments_then_returns_failure()
        {
            var result = _quoteArgumentParser.Parse(new [] { "arg1", "arg2", "arg3" });

            result.IsSuccessful.Should().BeFalse();
            result.Problems.ShouldBeEquivalentTo(new[] { "Expected 2 arguments: market data file, loan amount" });
        }

        [Fact]
        public void When_parsing_for_invalid_file_then_returns_failure()
        {
            var result = _quoteArgumentParser.Parse(new [] { "invalid-data.csv", "1000" });

            result.IsSuccessful.Should().BeFalse();
            result.Problems.ShouldBeEquivalentTo(new[] { "Market data file could not be read, or was in an incorrect format" });
        }

        [Fact]
        public void When_parsing_for_invalid_loan_amount_then_returns_failure()
        {
            var result = _quoteArgumentParser.Parse(new [] { "market.csv", "0" });

            result.IsSuccessful.Should().BeFalse();
            result.Problems.ShouldBeEquivalentTo(new[] { "Loan amount must be between 1000 and 15000" });
        }
    }
}
