﻿using FluentAssertions;
using Xunit;
using ZopaTechTest.ArgumentHandling;

namespace ZopaTechTest.Tests.ArgumentHandling
{
    public class LoanAmountParserTests
    {
        private readonly LoanAmountParser _loanAmountParser;

        public LoanAmountParserTests()
        {
            _loanAmountParser = new LoanAmountParser();
        }

        [Fact]
        public void When_parsing_a_valid_number_then_returns_success()
        {
            var result = _loanAmountParser.Parse("2000");

            result.IsSuccessful.Should().BeTrue();
            ((int)result.Outcome).Should().Be(2000);
        }

        [Theory]
        [InlineData("a string")]
        [InlineData("1.1")]
        [InlineData("100000000000000000000")]
        public void When_parsing_an_invalid_number_then_returns_correct_problem(string input)
        {
            var result = _loanAmountParser.Parse(input);

            result.IsSuccessful.Should().BeFalse();
            result.Problems.ShouldBeEquivalentTo(new[] {"Loan amount was not a valid number"});
        }

        [Theory]
        [InlineData("100")]
        [InlineData("999")]
        [InlineData("15001")]
        [InlineData("20000")]
        public void When_parsing_a_number_outside_the_valid_range_then_returns_correct_problem(string input)
        {
            var result = _loanAmountParser.Parse(input);

            result.IsSuccessful.Should().BeFalse();
            result.Problems.ShouldBeEquivalentTo(new[] {"Loan amount must be between 1000 and 15000"});
        }

        [Theory]
        [InlineData("1001")]
        [InlineData("1050")]
        [InlineData("10020")]
        public void When_parsing_a_number_within_the_valid_range_that_is_not_a_multiple_of_100_then_returns_correct_problem(string input)
        {
            var result = _loanAmountParser.Parse(input);

            result.IsSuccessful.Should().BeFalse();
            result.Problems.ShouldBeEquivalentTo(new[] {"Loan amount must be a multiple of 100"});
        }
    }
}
