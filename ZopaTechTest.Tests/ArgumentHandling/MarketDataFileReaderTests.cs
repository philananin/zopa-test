using FluentAssertions;
using Xunit;
using ZopaTechTest.ArgumentHandling;
using ZopaTechTest.Models;

namespace ZopaTechTest.Tests.ArgumentHandling
{
    public class MarketDataFileReaderTests
    {
        private readonly MarketDataFileReader _marketDataFileReader;

        public MarketDataFileReaderTests()
        {
            _marketDataFileReader = new MarketDataFileReader();
        }

        [Fact]
        public void When_valid_file_exists_then_returns_expected_market_data()
        {
            var result = _marketDataFileReader.ReadFile("market.csv");

            result.IsSuccessful.Should().BeTrue();
            result.Outcome.Should().BeEquivalentTo(
                new MarketData { Lender = "Bob", Rate = 0.075m, Available = 640 },
                new MarketData { Lender = "Jane", Rate = 0.069m, Available = 480 },
                new MarketData { Lender = "Fred", Rate = 0.071m, Available = 520 },
                new MarketData { Lender = "Mary", Rate = 0.104m, Available = 170 },
                new MarketData { Lender = "John", Rate = 0.081m, Available = 320 },
                new MarketData { Lender = "Dave", Rate = 0.074m, Available = 140 },
                new MarketData { Lender = "Angela", Rate = 0.071m, Available = 60 });
        }

        [Fact]
        public void When_file_does_not_exist_then_returns_expected_problem()
        {
            var result = _marketDataFileReader.ReadFile("non-existent-file.csv");

            result.IsSuccessful.Should().BeFalse();
            result.Problems.Should().BeEquivalentTo("Market data file does not exist");
        }

        [Fact]
        public void When_file_is_invalid_then_returns_expected_problem()
        {
            var result = _marketDataFileReader.ReadFile("invalid-data.csv");

            result.IsSuccessful.Should().BeFalse();
            result.Problems.Should().BeEquivalentTo("Market data file could not be read, or was in an incorrect format");
        }
    }
}