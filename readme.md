﻿#Zopa Tech Test

The application is written as a `netcoreapp2.0` application, meaning you will need .NET Core 2.0 installed.

## Problems encountered

If you run the tests (with the test project as the working directory):

```
dotnet test
```

You will see that we have two failures - in the end-to-end tests, and in the calculator tests. This is because the calculator does not calculate the exact answer that the spec was looking for. I have used the monthly compound interest formula, and haven't been able to figure out what causes this discrepancy.


## Building (Docker)

To build the application (from Powershell, with ZopaTechTest as the working directory):
```
./build.ps1
```

This will restore packages, build & publish the app, and create the Docker container.

To run the application using the sample market data:
```
./run-docker.ps1
```

This will run the example provided in the test specification.

To run a different value:
```
docker run zopa-tech-test market.csv 1500
```

NOTE: the market data file must be part of the docker image. This will not work with a different file. This is a limitation of Docker.

## Building (non-Docker)

To run the application:

```
dotnet restore
dotnet run market.csv 1000
```