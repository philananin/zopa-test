﻿using Ninject.Modules;

namespace ZopaTechTest.Infrastructure
{
    public class QuoteModule : NinjectModule
    {
        public override void Load()
        {
            // I had intended to wire up my IoC here, however my solution does not currently require wiring up!
        }
    }
}