﻿using System.Linq;
using ZopaTechTest.Models;

namespace ZopaTechTest.ArgumentHandling
{
    public class QuoteArgumentParser
    {
        private readonly LoanAmountParser _loanAmountParser;
        private readonly MarketDataFileReader _marketDataFileReader;

        public QuoteArgumentParser(LoanAmountParser loanAmountParser, MarketDataFileReader marketDataFileReader)
        {
            _loanAmountParser = loanAmountParser;
            _marketDataFileReader = marketDataFileReader;
        }

        public Result<QuoteArguments> Parse(string[] args)
        {
            if (args == null)
            {
                return Result<QuoteArguments>.Failure("Must provide arguments: market data file, loan amount");
            }

            if (args.Length != 2)
            {
                return Result<QuoteArguments>.Failure("Expected 2 arguments: market data file, loan amount");
            }

            var marketDataResult = _marketDataFileReader.ReadFile(args.First());

            if (!marketDataResult.IsSuccessful)
            {
                return Result<QuoteArguments>.Failure(marketDataResult.Problems);
            }

            var loanAmountResult = _loanAmountParser.Parse(args.Last());

            if (!loanAmountResult.IsSuccessful)
            {
                return Result<QuoteArguments>.Failure(loanAmountResult.Problems);
            }

            return Result<QuoteArguments>.Success(
                new QuoteArguments(marketDataResult.Outcome, loanAmountResult.Outcome));
        }
    }
}