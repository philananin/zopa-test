﻿using ZopaTechTest.Models;

namespace ZopaTechTest.ArgumentHandling
{
    public class LoanAmountParser
    {
        public Result<LoanAmount> Parse(string candidateAmount)
        {
            if (!int.TryParse(candidateAmount, out var amount))
            {
                return Result<LoanAmount>.Failure("Loan amount was not a valid number");
            }

            if (amount < 1000 || amount > 15000)
            {
                return Result<LoanAmount>.Failure("Loan amount must be between 1000 and 15000");
            }

            if (amount % 100 != 0)
            {
                return Result<LoanAmount>.Failure("Loan amount must be a multiple of 100");
            }

            return Result<LoanAmount>.Success(LoanAmount.Create(amount));
        }
    }
}