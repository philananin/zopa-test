﻿using System.Collections.Generic;
using ZopaTechTest.Models;

namespace ZopaTechTest.ArgumentHandling
{
    public class QuoteArguments
    {
        public QuoteArguments(IEnumerable<MarketData> marketData, LoanAmount loanAmount)
        {
            MarketData = marketData;
            LoanAmount = loanAmount;
        }

        public IEnumerable<MarketData> MarketData { get; }
        public LoanAmount LoanAmount { get; }
    }
}