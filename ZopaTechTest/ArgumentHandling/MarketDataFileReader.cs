﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using ZopaTechTest.Models;

namespace ZopaTechTest.ArgumentHandling
{
    public class MarketDataFileReader
    {
        public Result<IEnumerable<MarketData>> ReadFile(string filename)
        {
            if (!File.Exists(filename))
            {
                return Result<IEnumerable<MarketData>>.Failure("Market data file does not exist");
            }

            try
            {
                var csv = new CsvReader(File.OpenText(filename));
                var records = csv.GetRecords<MarketData>().ToList();
                return Result<IEnumerable<MarketData>>.Success(records);
            }
            catch
            {
                return Result<IEnumerable<MarketData>>.Failure(
                    "Market data file could not be read, or was in an incorrect format");
            }
        }
    }
}