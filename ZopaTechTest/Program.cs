﻿using System;
using Ninject;
using ZopaTechTest.Infrastructure;
using ZopaTechTest.QuoteHandling;

namespace ZopaTechTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var quoteSystem = GetEntryPoint();
            var quoteResult = quoteSystem.Run(args);

            if (!quoteResult.IsSuccessful)
            {
                Console.WriteLine($"Encountered the following problems: {string.Join(", ", quoteResult.Problems)}");
                return;
            }

            Console.WriteLine($"Requested amount: {quoteResult.Outcome.RequestedAmount}");
            Console.WriteLine($"Rate: {quoteResult.Outcome.Rate}");
            Console.WriteLine($"Monthly repayment: {quoteResult.Outcome.MonthlyRepayment}");
            Console.WriteLine($"Total repayment: {quoteResult.Outcome.TotalRepayment}");
        }

        private static QuoteSystem GetEntryPoint()
        {
            var kernel = new StandardKernel(new QuoteModule());
            return kernel.Get<QuoteSystem>();
        }
    }
}
