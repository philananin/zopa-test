﻿namespace ZopaTechTest.QuoteHandling
{
    public class Quote
    {
        public string RequestedAmount { get; }
        public string Rate { get; }
        public string MonthlyRepayment { get; }
        public string TotalRepayment { get; }

        public Quote(
            string requestedAmount,
            string rate,
            string monthlyRepayment,
            string totalRepayment)
        {
            RequestedAmount = requestedAmount;
            Rate = rate;
            MonthlyRepayment = monthlyRepayment;
            TotalRepayment = totalRepayment;
        }
    }
}