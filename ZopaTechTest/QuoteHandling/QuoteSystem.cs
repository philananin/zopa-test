﻿using ZopaTechTest.ArgumentHandling;
using ZopaTechTest.Models;

namespace ZopaTechTest.QuoteHandling
{
    public class QuoteSystem
    {
        private readonly QuoteArgumentParser _argumentParser;
        private readonly QuoteFulfilment _quoteFulfilment;
        private readonly QuoteCalculator _quoteCalculator;

        public QuoteSystem(QuoteArgumentParser argumentParser, QuoteFulfilment quoteFulfilment, QuoteCalculator quoteCalculator)
        {
            _argumentParser = argumentParser;
            _quoteFulfilment = quoteFulfilment;
            _quoteCalculator = quoteCalculator;
        }

        public Result<Quote> Run(params string[] args)
        {
            var argsResult = _argumentParser.Parse(args);
            if (!argsResult.IsSuccessful)
            {
                return Result<Quote>.Failure(argsResult.Problems);
            }

            var offersResult = _quoteFulfilment.GetOffersToFulfilLoan(
                argsResult.Outcome.LoanAmount, argsResult.Outcome.MarketData);

            if (!offersResult.IsSuccessful)
            {
                return Result<Quote>.Failure(offersResult.Problems);
            }

            var quote = _quoteCalculator.CalculateQuote(argsResult.Outcome.LoanAmount, offersResult.Outcome);
            return Result<Quote>.Success(quote);
        }
    }
}