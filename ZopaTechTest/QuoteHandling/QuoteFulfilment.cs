﻿using System.Collections.Generic;
using System.Linq;
using ZopaTechTest.Models;

namespace ZopaTechTest.QuoteHandling
{
    public class QuoteFulfilment
    {
        public Result<List<PartialQuote>> GetOffersToFulfilLoan(LoanAmount loanAmount, IEnumerable<MarketData> marketData)
        {
            var allMarketData = marketData.ToList();
            if (allMarketData.Sum(x => x.Available) < loanAmount)
            {
                return Result<List<PartialQuote>>.Failure("It is not possible to provide a quote at this time.");
            }

            var state = new QuoteState(loanAmount);
            var offersForQuote = allMarketData
                .OrderBy(x => x.Rate)
                .Aggregate(state, (acc, curr) =>
                {
                    acc.AddPartialQuote(curr.Rate, curr.Available, curr.Lender);
                    return acc;
                });

            return Result<List<PartialQuote>>.Success(offersForQuote.PartialQuotes);
        }

        private class QuoteState
        {
            private LoanAmount _remainingAmount;

            public QuoteState(LoanAmount loanAmount)
            {
                _remainingAmount = loanAmount;
                PartialQuotes = new List<PartialQuote>();
            }

            public List<PartialQuote> PartialQuotes { get; }

            public void AddPartialQuote(decimal rate, int amount, string lender)
            {
                if (_remainingAmount <= 0)
                {
                    return;
                }

                if (_remainingAmount < amount)
                {
                    PartialQuotes.Add(new PartialQuote(rate, _remainingAmount, lender));
                    _remainingAmount = LoanAmount.Create(0);
                }
                else
                {
                    PartialQuotes.Add(new PartialQuote(rate, amount, lender));
                    _remainingAmount = LoanAmount.Create(_remainingAmount - amount);
                }
            }
        }
    }
}