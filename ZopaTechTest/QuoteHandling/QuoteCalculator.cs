﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZopaTechTest.Formatting;
using ZopaTechTest.Models;

namespace ZopaTechTest.QuoteHandling
{
    public class QuoteCalculator
    {
        private const int LoanLengthInMonths = 36; // this should come from config

        public Quote CalculateQuote(LoanAmount loanAmount, List<PartialQuote> partialQuotes)
        {
            var rate = CalculateRate(partialQuotes, loanAmount);
            var monthlyRepayment = CalculateMonthlyRepayment(partialQuotes);
            var totalRepayment = monthlyRepayment * LoanLengthInMonths;

            return new Quote(
                loanAmount.FormatAsAmount(),
                rate.FormatAsRate(),
                monthlyRepayment.FormatAsCurrency(),
                totalRepayment.FormatAsCurrency());
        }

        private decimal CalculateRate(List<PartialQuote> partialQuotes, int loanAmount)
        {
            return partialQuotes.Aggregate(0m, (acc, curr) => acc + curr.Amount * curr.Rate) / loanAmount;
        }

        private decimal CalculateMonthlyRepayment(List<PartialQuote> partialQuotes)
        {
            return partialQuotes.Aggregate(
                0m,
                (acc, curr) => acc + CalculateLenderMonthlyRepayment((double) curr.Rate, curr.Amount));
        }

        private decimal CalculateLenderMonthlyRepayment(double rate, double amount)
        {
            var monthlyRate = rate / 12;
            return (decimal) (monthlyRate * amount / (1 - Math.Pow(1 + monthlyRate, -LoanLengthInMonths)));
        }
    }
}