﻿using ZopaTechTest.Models;

namespace ZopaTechTest.Formatting
{
    public static class LoanAmountExtensions
    {
        public static string FormatAsAmount(this LoanAmount amount)
        {
            // I would usually format as C0 for currency, but the required output does not include comma separators
            return $"£{(int)amount}";
        }
    }
}