﻿namespace ZopaTechTest.Formatting
{
    public static class DecimalExtensions
    {
        public static string FormatAsRate(this decimal rate)
        {
            var percentage = rate * 100;
            return $"{percentage:F1}%";
        }

        public static string FormatAsCurrency(this decimal amount)
        {
            return $"£{amount:F}";
        }
    }
}