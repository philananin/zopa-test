﻿namespace ZopaTechTest.Models
{
    public class PartialQuote
    {
        public PartialQuote(decimal rate, int amount, string lender)
        {
            Rate = rate;
            Amount = amount;
            Lender = lender;
        }

        public decimal Rate { get; }
        public int Amount { get; }
        public string Lender { get; }

        private bool Equals(PartialQuote other)
        {
            return Rate == other.Rate && Amount == other.Amount && string.Equals(Lender, other.Lender);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((PartialQuote) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Rate.GetHashCode();
                hashCode = (hashCode * 397) ^ Amount;
                hashCode = (hashCode * 397) ^ (Lender != null ? Lender.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}