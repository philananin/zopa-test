﻿namespace ZopaTechTest.Models
{
    public class MarketData
    {
        public string Lender { get; set; }
        public decimal Rate { get; set; }
        public int Available { get; set; }

        private bool Equals(MarketData other)
        {
            return string.Equals(Lender, other.Lender) && Rate == other.Rate && Available == other.Available;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((MarketData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Lender != null ? Lender.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Rate.GetHashCode();
                hashCode = (hashCode * 397) ^ Available;
                return hashCode;
            }
        }
    }
}