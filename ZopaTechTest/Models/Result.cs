﻿using System.Linq;

namespace ZopaTechTest.Models
{
    public class Result<T> where T : class
    {
        private Result(T outcome, string[] problems)
        {
            Outcome = outcome;
            Problems = problems;
        }

        public static Result<T> Success(T outcome)
        {
            return new Result<T>(outcome, new string[0]);
        }

        public static Result<T> Failure(params string[] problems)
        {
            return new Result<T>(null, problems);
        }

        public T Outcome { get; }
        public string[] Problems { get; }

        public bool IsSuccessful => !Problems?.Any() ?? false;
    }
}