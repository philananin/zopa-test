﻿namespace ZopaTechTest.Models
{
    public class LoanAmount
    {
        private readonly int _amount;

        private LoanAmount(int amount)
        {
            _amount = amount;
        }

        public static LoanAmount Create(int amount)
        {
            return new LoanAmount(amount);
        }

        public static implicit operator int(LoanAmount loanAmount)
        {
            return loanAmount._amount;
        }
    }
}